<?php
$filename = basename($_FILES['uploadedfile']['name']);
$username = $_POST['username'];
$path="/srv/uploads/".$username;
$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
if(!is_dir($path))
{
    mkdir($path);
}

if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
	header("Location: home.php?msg=success&username=".$username);
	exit;
}else{
	header("Location: home.php?msg=error&username=".$username);
	exit;
}

?>

